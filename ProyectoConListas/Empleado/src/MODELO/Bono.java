/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MODELO;

/**
 *
 * @author Dennys
 */
public class Bono {
    private Float porcentajeDeBono;

    public Float getPorcentajeDeBono() {
        return porcentajeDeBono;
    }

    public void setPorcentajeDeBono(Float porcentajeDeBono) {
        this.porcentajeDeBono = porcentajeDeBono;
    }
    
}

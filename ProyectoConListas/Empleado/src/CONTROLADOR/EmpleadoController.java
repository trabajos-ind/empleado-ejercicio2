/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package CONTROLADOR;

import CONTROLADOR.Listas.ListaEnlazada;
import MODELO.Bono;
import MODELO.Empleado;
import MODELO.TipoDeEmpleado;
import java.util.Date;

/**
 *
 * @author Dennys
 */
public class EmpleadoController {

    private ListaEnlazada<Empleado> listaempleado;

    public EmpleadoController(ListaEnlazada<Empleado> listaempleado) {
        this.listaempleado = listaempleado;
    }
    
    
    
    public Date calcularAniosEmpleo(Date fechaIngreso){
        Date aux=new Date();
        aux.setYear(aux.getYear()+1900);
        aux.setYear(aux.getYear()-fechaIngreso.getYear());
        return aux;
    }
    
    public void DefinirTipoDeEmpleado(Empleado empleado){
        Date fechaIngreso=calcularAniosEmpleo(empleado.getFechaDeIngreso());
        if (fechaIngreso.getYear()>15) {
            empleado.setTipoDeEmpleado(TipoDeEmpleado.veterano);
        }
        if (fechaIngreso.getYear()<15 & fechaIngreso.getYear()>5) {
            empleado.setTipoDeEmpleado(TipoDeEmpleado.senior);
        } 
        if (fechaIngreso.getYear()<5) {
            empleado.setTipoDeEmpleado(TipoDeEmpleado.junior);
        }
    }
    
    public void definirBono(Empleado empleado){
        Bono bono=new Bono();
        switch (empleado.getTipoDeEmpleado()) {
            case veterano:
                bono.setPorcentajeDeBono(20/100f);
                empleado.setBono(bono);
                break;
            case senior :
                bono.setPorcentajeDeBono(10/100f);
                empleado.setBono(bono);
                break;
            case junior:
                bono.setPorcentajeDeBono(10/100f);
                empleado.setBono(bono);
                break;
            default:
                throw new AssertionError();
        }
    }
    
    public void calcularSueldoEmpleado(Empleado empleado){
        empleado.setSueldo(empleado.getSueldo()+empleado.getSueldo()*empleado.getBono().getPorcentajeDeBono());
        System.out.println("Sueldo: "+empleado.getSueldo());
    }

    public ListaEnlazada<Empleado> getListaempleado() {
        return listaempleado;
    }

    public void setListaempleado(ListaEnlazada<Empleado> listaempleado) {
        this.listaempleado = listaempleado;
    }
    
    
    
    
}

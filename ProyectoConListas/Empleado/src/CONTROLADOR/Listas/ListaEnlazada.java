/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLADOR.Listas;

/**
 *
 * @author Edison
 */
public class ListaEnlazada <E>{
    //El primer de elemento de la lista se llama cabecera y el ultimo la cola
    //Cuando es una lista de un solo elemento es cabecera y cola
    //Las listas circulares son las unicas que no apuntan a null
    private NodoLista <E> cabecera;
    private Integer size=0 ;
    
    public ListaEnlazada(){
    cabecera = null;
    }


    public NodoLista<E> getCabecera() {
        return cabecera;
    }

    public void setCabecera(NodoLista<E> cabecera) {
        this.cabecera = cabecera;
    }

    public Integer getSize() {
       // this.size = tamanio();
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
    

    
    public Boolean estaVacia(){
    return cabecera == null;
    }
    
//    private Integer tamanio(){
//     Integer tamanio = 0;
//     NodoLista<E> aux = cabecera;
//        while (aux !=null) {
//            tamanio++;
//            aux = aux.getSiguiente();
//            
//        }
//     return tamanio;
//    }
    public void insertar(E dato){
       NodoLista<E> nodo = new NodoLista<>(dato, null);
        if (estaVacia()) {
            this.cabecera = nodo;

        } else {
            NodoLista<E> aux = cabecera;
            while (aux.getSiguiente() != null) {
                aux = aux.getSiguiente();
            }
            aux.setSiguiente(nodo);
        }
        size++;
    }
    
    public void insertarCabecera(E dato){
        if (estaVacia()) {
            insertar(dato);
        } else {
            NodoLista<E> nodo = new NodoLista<>(dato, null);
            nodo.setSiguiente(cabecera);
            cabecera = nodo;
            size++;

        }
       
    }
    
    public void insertarPosicion(E dato, Integer pos){
        if (estaVacia()) {
            insertar(dato);
        }else if(pos >= 0 && pos < size){ 
            if (pos == 0) {
                insertarCabecera(dato);
            }else{
            NodoLista<E> nodo = new NodoLista(dato,null);
            NodoLista<E> aux = cabecera;
            
            for (int i = 0; i < (pos -1); i++) {
                aux = aux.getSiguiente();
            }
            NodoLista<E> siguiente = aux.getSiguiente();
            aux.setSiguiente(nodo);
            nodo.setSiguiente(siguiente);
            size++;
            }
            
      }
    }
    public void imprimir(){
        System.out.println("--------------------LISTA ENLAZADA------------------------");
        NodoLista<E> aux = cabecera;
        while (aux != null) {
            System.out.println(aux.getDato().toString() + "\t");
            aux = aux.getSiguiente();
        }
        System.out.println("\n----------------------------------------------------------");

    }
    
    public E obtener(Integer pos){
        if (estaVacia()) {
            E dato = null;
            if (pos >= 0 && pos < size) {
                if(pos == 0){
                    dato = cabecera.getDato();
                }else{
                     NodoLista<E> aux = cabecera;
                     for(int i = 0; i < pos; i++){
                         aux = aux.getSiguiente();
                     }
                     dato = aux.getDato();
                }
            return dato;
            }
    }           
        return null;
    }
}
  
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Main;

import CONTROLADOR.EmpleadoController;
import CONTROLADOR.Listas.ListaEnlazada;
import CONTROLADOR.Listas.NodoLista;
import EXCEPTIONS.ArrayEmpleadoLLenoException;
import MODELO.Empleado;
import java.util.Date;

/**
 *
 * @author Dennys
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
        Date nuevo=new Date(2019, 10, 15);
        
        ListaEnlazada<Empleado> listPrueba=new ListaEnlazada<>();
        EmpleadoController ec=new EmpleadoController(listPrueba);
        System.out.println("fecha actual --> "+ nuevo.toString());
        
        
        ec.calcularAniosEmpleo(nuevo);
        System.out.println("fecha calculada -->"+ ec.calcularAniosEmpleo(nuevo));
        
        Empleado e1=new Empleado();
        Empleado e2=new Empleado();
        Empleado e3=new Empleado();
        
        e1.setFechaDeIngreso(nuevo);
        e1.setNombre("Juan");
        e2.setNombre("Pedro");
        e3.setNombre("Jhon");
        
        
        listPrueba.insertar(e1);
        listPrueba.insertar(e2);
        listPrueba.insertar(e3);
        
        
        
        ec.setListaempleado(listPrueba);
        ec.getListaempleado().imprimir();
    }
    
}

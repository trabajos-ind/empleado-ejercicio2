/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VISTA.modeloTabla;

import CONTROLADOR.EmpleadoController;
import CONTROLADOR.Listas.ListaEnlazada;
import javax.swing.table.AbstractTableModel;
import MODELO.Empleado;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sebastian
 */
public class ModeloTablaEmpleado extends AbstractTableModel {
    private EmpleadoController ec;

    public EmpleadoController getEc() {
        return ec;
    }

    public void setEc(EmpleadoController ec) {
        this.ec = ec;
    }

    

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public int getRowCount() {
        return ec.getListaempleado().getSize();
    }

    @Override
    public String getColumnName(int i) {
        
        switch(i) {
            case 0: return "Nombre";
            case 1: return "Genero";    
            case 2: return "Edad";
            case 3: return "Bono";
            case 4: return "Tipo de Empleado";
            case 5: return "Sueldo";
            case 6: return "Pagado";
            case 7: return "Ingreso";
            default: return null;
        }
    }

    @Override
    public Object getValueAt(int i, int i1) {
            Empleado e;
            e=ec.getListaempleado().obtener(i);
            //ec.getListaempleado().imprimir();
            return switch (i1) {
                case 0 -> (e != null) ? e.getNombre() : "NO DEFINIDO";
                case 1 -> (e != null) ? e.getGenero() : "NO DEFINIDO";
                case 2 -> (e != null) ? e.getEdad() : "NO DEFINIDO";
                case 3 -> (e != null) ? e.getBono().getPorcentajeDeBono() : "NO DEFINIDO";
                case 4 -> (e != null) ? e.getTipoDeEmpleado() : "NO DEFINIDO";
                case 5 -> (e != null) ? e.getSueldo() : "NO DEFINIDO";
                case 6 -> (e != null) ? e.getEstadoDePago() : "NO DEFINIDO";
                case 7 -> (e != null) ? e.getFechaDeIngreso() : "NO DEFINIDO";
                default -> null;
            };
        }
        
}

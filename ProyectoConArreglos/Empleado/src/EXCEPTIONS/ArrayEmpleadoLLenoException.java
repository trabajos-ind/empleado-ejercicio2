/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package EXCEPTIONS;

/**
 *
 * @author Dennys
 */
public class ArrayEmpleadoLLenoException extends Exception{
    public ArrayEmpleadoLLenoException(String msg){
    super (msg);
    }
    public ArrayEmpleadoLLenoException(){
    super ("El array de empleado se encuentra lleno");
    }
}

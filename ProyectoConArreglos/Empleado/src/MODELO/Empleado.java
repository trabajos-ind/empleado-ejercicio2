/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package MODELO;

import java.util.Date;

/**
 *
 * @author Dennys
 */
public class Empleado extends Persona{
    private Integer id_Empleado;
    private Float sueldo;
    private TipoDeEmpleado tipoDeEmpleado;
    private Bono bono;
    private Date fechaDeIngreso;
    private Boolean estadoDePago;

    public Empleado() {
    }
    
    public Integer getId_Empleado() {
        return id_Empleado;
    }

    public void setId_Empleado(Integer id_Empleado) {
        this.id_Empleado = id_Empleado;
    }

    public Float getSueldo() {
        return sueldo;
    }

    public void setSueldo(Float sueldo) {
        this.sueldo = sueldo;
    }

    public Boolean getEstadoDePago() {
        return estadoDePago;
    }

    public void setEstadoDePago(Boolean estadoDePago) {
        this.estadoDePago = estadoDePago;
    }

    

    public TipoDeEmpleado getTipoDeEmpleado() {
        return tipoDeEmpleado;
    }

    public void setTipoDeEmpleado(TipoDeEmpleado tipoDeEmpleado) {
        this.tipoDeEmpleado = tipoDeEmpleado;
    }

    public Bono getBono() {
        return bono;
    }

    public void setBono(Bono bono) {
        this.bono = bono;
    }

    public Date getFechaDeIngreso() {
        return fechaDeIngreso;
    }

    public void setFechaDeIngreso(Date fechaDeIngreso) {
        this.fechaDeIngreso = fechaDeIngreso;
    }
    
    
}

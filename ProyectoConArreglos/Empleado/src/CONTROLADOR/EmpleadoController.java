/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package CONTROLADOR;

import EXCEPTIONS.ArrayEmpleadoLLenoException;
import MODELO.Bono;
import MODELO.Empleado;
import MODELO.TipoDeEmpleado;
import java.util.Date;

/**
 *
 * @author Dennys
 */
public class EmpleadoController {
    private Empleado[] arrayEmpleado;
    private Empleado empleado;

    public EmpleadoController(int tamanio) {
        Empleado[] em=new Empleado[tamanio];
        arrayEmpleado=em;
    }
    
    public boolean estaLleno() {
        return arrayEmpleado.length <= verificarPosicion() + 1;
    }
    
    public int verificarPosicion() {
        int pos = -1;//este vacio
        //0 hasta limite real
        for(int i = 0; i < arrayEmpleado.length;i++) {
            if(arrayEmpleado[i] == null) {
                break;
            } else {
                pos = i;
            }
        }
        return pos;
    }
    
    public boolean guardarEmpleado(Empleado empleado) throws ArrayEmpleadoLLenoException {
        if(estaLleno()) {
            throw new ArrayEmpleadoLLenoException("El arreglo esta lleno");
        } else {
            arrayEmpleado[verificarPosicion() + 1]=empleado;
            return true;
        }
        
    }
    
    public Date calcularAniosEmpleo(Date fechaIngreso){
        Date aux=new Date();
        aux.setYear(aux.getYear()+1900);
        aux.setYear(aux.getYear()-fechaIngreso.getYear());
        return aux;
    }
    
    public void ImprimirArreglo(){
        for (int i = 0; i < arrayEmpleado.length; i++) {
            System.out.println("Empleado "+i+": "+arrayEmpleado[i]);
        }
    }

    public void DefinirTipoDeEmpleado(Empleado empleado){
        Date fechaIngreso=calcularAniosEmpleo(empleado.getFechaDeIngreso());
        if (fechaIngreso.getYear()>15) {
            empleado.setTipoDeEmpleado(TipoDeEmpleado.veterano);
        }
        if (fechaIngreso.getYear()<15 & fechaIngreso.getYear()>5) {
            empleado.setTipoDeEmpleado(TipoDeEmpleado.senior);
        } 
        if (fechaIngreso.getYear()<5) {
            empleado.setTipoDeEmpleado(TipoDeEmpleado.junior);
        }
    }
    
    public void definirBono(Empleado empleado){
        Bono bono=new Bono();
        switch (empleado.getTipoDeEmpleado()) {
            case veterano:
                bono.setPorcentajeDeBono(20/100f);
                empleado.setBono(bono);
                break;
            case senior :
                bono.setPorcentajeDeBono(10/100f);
                empleado.setBono(bono);
                break;
            case junior:
                bono.setPorcentajeDeBono(10/100f);
                empleado.setBono(bono);
                break;
            default:
                throw new AssertionError();
        }
    }
    
    public void calcularSueldoEmpleado(Empleado empleado){
        empleado.setSueldo(empleado.getSueldo()+empleado.getSueldo()*empleado.getBono().getPorcentajeDeBono());
        System.out.println("Sueldo: "+empleado.getSueldo());
    }
    
    public Empleado[] getArrayEmpleado() {
        return arrayEmpleado;
    }

    public void setArrayEmpleado(Empleado[] arrayEmpleado) {
        this.arrayEmpleado = arrayEmpleado;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VISTA.modeloTabla;

import CONTROLADOR.EmpleadoController;
import javax.swing.table.AbstractTableModel;
import MODELO.Empleado;

/**
 *
 * @author sebastian
 */
public class ModeloTablaEmpleado extends AbstractTableModel {
    private EmpleadoController em;

    public EmpleadoController getEm() {
        return em;
    }

    public void setEm(EmpleadoController em) {
        this.em = em;
    }


    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public int getRowCount() {
        return em.getArrayEmpleado().length;
    }

    @Override
    public String getColumnName(int i) {
        
        switch(i) {
            case 0: return "Nombre";
            case 1: return "Genero";    
            case 2: return "Edad";
            case 3: return "Bono";
            case 4: return "Tipo de Empleado";
            case 5: return "Sueldo";
            case 6: return "Pagado";
            case 7: return "Ingreso";
            default: return null;
        }
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Empleado e=em.getArrayEmpleado()[i];
        switch(i1) {
            case 0: return (e != null) ? e.getNombre() : "NO DEFINIDO";
            case 1: return (e != null) ? e.getGenero() : "NO DEFINIDO";    
            case 2: return (e != null) ? e.getEdad() : "NO DEFINIDO";
            case 3: return (e != null) ? e.getBono().getPorcentajeDeBono() : "NO DEFINIDO";
            case 4: return (e != null) ? e.getTipoDeEmpleado() : "NO DEFINIDO";
            case 5: return (e != null) ? e.getSueldo() : "NO DEFINIDO";
            case 6: return (e != null) ? e.getEstadoDePago() : "NO DEFINIDO";
            case 7: return (e != null) ? e.getFechaDeIngreso() : "NO DEFINIDO";
            default: return null;
        }
    }
    
    
    
    
}

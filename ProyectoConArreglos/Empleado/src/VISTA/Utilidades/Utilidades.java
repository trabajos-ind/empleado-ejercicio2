/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VISTA.Utilidades;

import CONTROLADOR.EmpleadoController;
import MODELO.TipoDeEmpleado;
import MODELO.TipoDeGenero;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.swing.JComboBox;

/**
 *
 * @author dennys
 */
public class Utilidades {


    //public static String DIRCARPDATA="data";
public static JComboBox cargarComboTipoEmpleado(JComboBox cbx){
        cbx.removeAllItems();
        for(TipoDeEmpleado tipo: TipoDeEmpleado.values()) {
            cbx.addItem(tipo);
        }
        return cbx;
}
    
public static TipoDeEmpleado getTipoEmpleadoCOmbo(JComboBox  cbx){
    
    return (TipoDeEmpleado)cbx.getSelectedItem();
}
   
public static JComboBox cargarComboTipoGenero(JComboBox cbx){
        cbx.removeAllItems();
        for(TipoDeGenero tipo: TipoDeGenero.values()) {
            cbx.addItem(tipo);
        }
        return cbx;
}
    
public static TipoDeGenero getTipoGeneroCOmbo(JComboBox  cbx){
    
    return (TipoDeGenero)cbx.getSelectedItem();
}

public static boolean guardarEnJSON(EmpleadoController ec) throws IOException{
    ObjectMapper JSON_MAPPER = new ObjectMapper();
    try {
        JSON_MAPPER.writeValue(new File("data/empleado.json"), ec);
        return true;
    } catch (Exception e) {
        System.out.println("Error"+e);
        return false;
    }
}

public static EmpleadoController cargarJson() throws JsonProcessingException, IOException{
    ObjectMapper JSON_MAPPER = new ObjectMapper();
    File file=new File("data/empleado.json");
    EmpleadoController ec;
    ec=(EmpleadoController)JSON_MAPPER.readValue(file, EmpleadoController.class);
    return ec;
}

//public static boolean guardarArchivo(Bolsa[] bolsas){
//    try {
//        File archivo=new File(DIRCARPDATA+File.separatorChar+"bolsa.dat");
//        FileOutputStream salida=new FileOutputStream(archivo);
//        ObjectOutputStream ous=new ObjectOutputStream(salida);
//        ous.writeObject(bolsas);
//        ous.flush();
//        ous.close();
//        salida.close();
//        return true;
//    } catch (Exception e) {
//        System.out.println("Error"+e);
//        return false;
//    }
//}
//
//public static Bolsa[] cargarArchivo(){
//    Bolsa[] bolsas=null;
//    try {
//        File archivo=new File(DIRCARPDATA+File.separatorChar+"bolsa.dat");
//        FileInputStream entrada=new FileInputStream(archivo);
//        ObjectInputStream ous=new ObjectInputStream(entrada);
//        //ous.writeObject(bolsas);
//        bolsas=(Bolsa[])ous.readObject();
//        ous.close();
//        entrada.close();
//    } catch (Exception e) {
//        System.out.println("Error"+e);
//    }
//    return bolsas;
//}
//
//public static boolean GuardarArchivoXML(bolsaArray c){
//    try {
//        //Bolsa aux=new Bolsa();
//        JAXBContext contexto=JAXBContext.newInstance(c.getClass());
//        Marshaller marshaller=contexto.createMarshaller();
//        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//        File fileXML =new File(DIRCARPDATA+File.separatorChar+"bolsa.xml");
//        
//        marshaller.marshal(c, fileXML);
//        
//        return true;
//    } catch (Exception e) {
//        System.out.println("Error"+e);
//        return false;
//    }
//}
//
//public static bolsaArray cargarArchivoXML(bolsaArray c){
//    try {
//            JAXBContext contexto=JAXBContext.newInstance(c.getClass());
//            Unmarshaller unmarshaller = contexto.createUnmarshaller();
//            File fileXML =new File(DIRCARPDATA+File.separatorChar+"bolsa.xml");
//            c = (bolsaArray)unmarshaller.unmarshal(fileXML);
//        
//    } catch (Exception e) {
//        System.out.println("Error"+e);
//    }
//    return c;
//}



}


/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Main;

import CONTROLADOR.EmpleadoController;
import EXCEPTIONS.ArrayEmpleadoLLenoException;
import MODELO.Empleado;
import java.util.Date;

/**
 *
 * @author Dennys
 */
public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ArrayEmpleadoLLenoException {
        Date nuevo=new Date(2019, 10, 15);
        System.out.println("fecha actual --> "+ nuevo.toString());
        
        EmpleadoController ec=new EmpleadoController(5);
        ec.calcularAniosEmpleo(nuevo);
        System.out.println("fecha calculada -->"+ ec.calcularAniosEmpleo(nuevo));
        
        Empleado e1=new Empleado();
        Empleado e2=new Empleado();
        Empleado e3=new Empleado();
        e1.setNombre("Juan");
        e2.setNombre("Pedro");
        e3.setNombre("Jhon");
        
        ec.guardarEmpleado(e1);
        ec.guardarEmpleado(e2);
        ec.guardarEmpleado(e3);
        
        ec.ImprimirArreglo();
        
        e1.setFechaDeIngreso(nuevo);
        ec.DefinirTipoDeEmpleado(e1);
        System.out.println("Es un -->"+ ec.getArrayEmpleado()[0].getTipoDeEmpleado());
        
        ec.definirBono(e1);
        ec.getArrayEmpleado()[0].setSueldo(16000.0f);
        ec.calcularSueldoEmpleado(e1);
    }
    
}
